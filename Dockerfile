FROM php:7.4-fpm

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y locales \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && sed -i -e 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=ru_RU.UTF-8

ENV LANG ru_RU.UTF-8 
ENV LC_ALL ru_RU.UTF-8
ENV COMPOSER_ALLOW_SUPERUSER 1 \
    TZ ${TZ:-"Europe/Moscow"} 

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libjpeg-dev \
        libpng-dev \
        libicu-dev \
        libtidy-dev \
        libzip-dev \
        zlibc \
        zlib1g \
        build-essential libz-dev \
        python-dev libxml2-dev libxslt1-dev zlib1g-dev ssh \
        mc vim sudo tzdata git zip wget curl \
        nodejs npm \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip \
    && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include \
    && docker-php-ext-install -j$(nproc) gd \
    && pecl install redis-5.2.2 \
    && docker-php-ext-install \
            intl \
            mysqli \
            pdo \
            pdo_mysql \
            soap \
            tidy \
            xsl \
            zip \
            opcache \
            pcntl \
            sockets \
            gettext \
            exif \
    && docker-php-ext-enable exif redis \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY php.ini "$PHP_INI_DIR/php.ini"

RUN userdel -f www-data
RUN if getent group www-data ; then groupdel www-data; fi
RUN groupadd www-data
RUN useradd -l -g www-data -m -d /home/www www-data

RUN wget http://getcomposer.org/installer \
    && php installer -- --install-dir=/usr/bin --filename=composer \
    && rm installer \
    && composer global require hirak/prestissimo \
    && composer global require phploc/phploc \
    && composer global require laravel/installer

RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN mv /root/.composer  /home/www/.composer

RUN npm install -g yarn

EXPOSE 9000

WORKDIR "/srv/site"

